import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_clock_step(host, ntp_service):
    ntp_date = host.run('date+"%y')
    assert ntp_date.rc != 12
