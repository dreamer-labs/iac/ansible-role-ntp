import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_ntp_package_installed(host, ntp_package):
    assert host.package(ntp_package).is_installed


def test_ntp_service_running(host, ntp_service):
    assert host.service(ntp_service).is_running
    assert host.service(ntp_service).is_enabled
