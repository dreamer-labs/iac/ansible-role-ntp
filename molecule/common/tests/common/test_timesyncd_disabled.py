import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('debian')


def test_timesyncd_disabled(host):
    assert not host.service('systemd-timesyncd').is_enabled
    assert not host.service('systemd-timesyncd').is_running
