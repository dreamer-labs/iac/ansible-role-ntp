ansible-role-ntp
================

This role configures and installs an NTP client on a target system.
If the target system is debian and has systemd-timesyncd installed
we will uninstall it and install NTP/Chrony instead.

## Requirements

None

## Role Variables

**ntp_servers**
  * Type: List
  * Default: ['0.centos.pool.ntp.org', '1.centos.pool.ntp.org']
  * A list of NTP servers to configure

**ntp_service**
  * Type: Dict
  * Required: False
  * A dictionary containing the package to install and the service to configure. Check `vars/main.yml`
  * Example:
    ```
    ntp_service:
      package: chrony
      service: chronyd
    ```

**ntp_drift_file**
  * Type: String
  * Required: False
  * Defaults to `/var/lib/ntp/drift`

**ntp_restrict**
  * Type: List
  * Required: False
  * A list of restrictions for the ntp client. Check NTP documentation
  * Example:
    ```
    ntp_restrict:
      - default nomodify notrap nopeer noquery
      - 127.0.0.1
      - ::1
