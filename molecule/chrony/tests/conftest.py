import pytest


@pytest.fixture
def ntp_package():
    return "chrony"


@pytest.fixture
def ntp_service(host):
    os_family = host.ansible("setup")["ansible_facts"]["ansible_os_family"]
    if os_family == "Debian":
        return "chrony"
    elif os_family == "RedHat":
        return "chronyd"
