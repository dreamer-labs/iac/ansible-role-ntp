import pytest


@pytest.fixture
def ntp_package():
    return 'ntp'


@pytest.fixture
def ntp_service(host):
    os_family = host.ansible("setup")["ansible_facts"]["ansible_os_family"]
    if os_family == "Debian":
        return 'ntp'
    elif os_family == "RedHat":
        return 'ntpd'
